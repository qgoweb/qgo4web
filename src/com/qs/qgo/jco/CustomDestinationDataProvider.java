package com.qs.qgo.jco;

import java.util.Properties;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.ext.DataProviderException;
import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;

public class CustomDestinationDataProvider {

	public static void main(String[] args) {
		MyDestinationDataProvider myProvider = new MyDestinationDataProvider();
		try {
			com.sap.conn.jco.ext.Environment.registerDestinationDataProvider(myProvider);
		} catch (IllegalStateException e) {
			throw new Error(e);
		}

		String destName = "/QGO/WS_UNIT_GET_LIST";
		CustomDestinationDataProvider test = new CustomDestinationDataProvider();
		test.executeCalls(destName);
	}	
	
	static class MyDestinationDataProvider implements DestinationDataProvider {
		public Properties getDestinationProperties(String destinationName) {
			try {
				Properties p = new Properties();
				p.setProperty(DestinationDataProvider.JCO_ASHOST, "10.1.3.7");
				p.setProperty(DestinationDataProvider.JCO_SYSNR, "00");
				p.setProperty(DestinationDataProvider.JCO_CLIENT, "800");
				p.setProperty(DestinationDataProvider.JCO_USER, "bknobel");
				p.setProperty(DestinationDataProvider.JCO_PASSWD, "sap123");
				p.setProperty(DestinationDataProvider.JCO_LANG, "en");

				return p;
			} catch (RuntimeException re) {
				throw new DataProviderException(DataProviderException.Reason.INTERNAL_ERROR, re);
			}
		}

		public void setDestinationDataEventListener(DestinationDataEventListener eventListener) {
		}

		public boolean supportsEvents() {
			return true;
		}

	}

	void executeCalls(String destName) {
		try {
			JCoDestination dest = JCoDestinationManager.getDestination(destName);
			dest.ping();
			System.out.println("Destination " + destName + " works");
		} catch (JCoException e) {
			e.printStackTrace();
			System.out.println("Execution on destination " + destName + " failed");
		}
	}
}

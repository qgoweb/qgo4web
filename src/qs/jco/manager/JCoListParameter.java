package qs.jco.manager;

public final class JCoListParameter {

    public static final String EXPORT_PARAMETERS_LIST = "export_parameters_list";
    public static final String TABLE_PARAMETERS_LIST = "table_parameter_list";

    private JCoListParameter() { }
}

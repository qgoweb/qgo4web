package qs.qgo.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.qs.qgo.repository.MenuListRepository;
import com.qs.qgo.repository.MenuListRepositoryImpl;

import qs.jco.service.SingletonComponent;

@Path("/menu")
public class MenuList {

	@SingletonComponent
	private MenuListRepository menuListRepository;

	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String list() {
		menuListRepository = new MenuListRepositoryImpl();
		String response = menuListRepository.getMenuList();
		return response;
	}
}
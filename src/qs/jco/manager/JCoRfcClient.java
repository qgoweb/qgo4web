package qs.jco.manager;

import java.util.Map;

import com.sap.conn.jco.JCoRecord;

public interface JCoRfcClient {
	Map<String, JCoRecord> executeRfc(String bapi, Map<String, Object> inParams);
}

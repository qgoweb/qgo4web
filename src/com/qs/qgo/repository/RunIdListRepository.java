package com.qs.qgo.repository;

public interface RunIdListRepository {
	String getRunIdList(Object unitName);
}

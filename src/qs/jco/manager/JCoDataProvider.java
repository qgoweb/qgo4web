package qs.jco.manager;

import java.io.IOException;
import java.util.Properties;

import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;

public class JCoDataProvider implements DestinationDataProvider {
	
	public static final String RFC_SYSTEM_ID = "ED1";
	private Properties props;

	@Override
	public Properties getDestinationProperties(String name) {
		if (name.equals(RFC_SYSTEM_ID)) {
			if (props == null) {
				try {
					props = new Properties();
					props.load(getClass().getClassLoader().getResourceAsStream("jco.properties"));
				} catch (IOException ex) {
					throw new RuntimeException(
							"Cannot load JCo properties for destination " + name + ". " + ex.getMessage());
				}
			}
		} else {
			throw new IllegalArgumentException("Not configured for " + name);
		}
		return props;
	}

	@Override
	public boolean supportsEvents() {
		return true;
	}

	@Override
	public void setDestinationDataEventListener(DestinationDataEventListener dl) {
	}
}

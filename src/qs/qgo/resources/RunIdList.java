package qs.qgo.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qs.qgo.repository.RunIdListRepository;
import com.qs.qgo.repository.RunIdListRepositoryImpl;

import qs.jco.service.SingletonComponent;

@Path("/runids")
public class RunIdList {

	@SingletonComponent
	private RunIdListRepository runIdLisstRepository;

	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list(@QueryParam("unit") String unit) {
		runIdLisstRepository = new RunIdListRepositoryImpl();
		String response = runIdLisstRepository.getRunIdList(unit);
		return response;
	}
}

package com.qs.qgo.repository;

import java.util.HashMap;
import java.util.Map;

import qs.jco.manager.JCoManager;
import qs.jco.manager.JCoManagerImpl;

public class ChainRunListRepositoryImpl implements ChainRunListRepository {
	private static final String CHAINLIST_FUNCTION = "/QGO/WS_STAT_B11_GET_LIST";
	private static final String CHAINLIST_UNIT_PARAM_NAME = "X_UNIT";
	private static final String CHAINLIST_RUNID_PARAM_NAME = "X_RUNID";
	private static final String CHAINLIST_PROCESS_TYPE_PARAM_NAME = "X_PT";
	private static final String CHAILLIST_JSON_PARAM = "YS_LIST_STAT_B11_JSON";
	private JCoManager jcoManager = new JCoManagerImpl();
	
	@Override
	public String getChainRunList(Object unitName, Object runId, Object processType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CHAINLIST_UNIT_PARAM_NAME, unitName);
		params.put(CHAINLIST_RUNID_PARAM_NAME, runId);
		params.put(CHAINLIST_PROCESS_TYPE_PARAM_NAME, processType);
		return jcoManager.getData(CHAINLIST_FUNCTION, CHAILLIST_JSON_PARAM, params);
	}

}

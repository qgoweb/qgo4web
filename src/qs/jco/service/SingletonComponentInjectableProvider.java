package qs.jco.service;

import java.lang.reflect.Type;

import javax.ws.rs.ext.Provider;

import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.InjectableProvider;

@Provider
public class SingletonComponentInjectableProvider
		implements InjectableProvider<SingletonComponent, Type>, Injectable<Object> {
	
	private Type type;

	@Override
	public ComponentScope getScope() {
		return ComponentScope.Singleton;
	}

	@Override
	public Injectable getInjectable(ComponentContext ctx, SingletonComponent annot, Type type) {
		this.type = type;
		return this;
	}

	@Override
	public Object getValue() {
		Object obj = null;
		try {
			obj = ((Class) type).newInstance();
		} catch (InstantiationException ex) {
			throw new RuntimeException(ex);
		} catch (IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
		return obj;
	}
}

package qs.qgo.resources;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// START: TEMP modification for Google Assistant, Delete this after test
public class AlexaSkill extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String x = "{\"version\": \"1.0\",\"sessionAttributes\": {},\"response\": {\"outputSpeech\": {\"type\": \"PlainText\",\"text\": \"keinIntent\"},\"card\": {\"type\": \"Simple\",\"title\": \"CardTitle\",\"content\": \"CardText\"},\"reprompt\": {\"outputSpeech\": {\"type\": \"PlainText\",\"text\": \"Test\"	}},\"shouldEndSession\": \"True\"}}";

		res.setContentType("application/json");
		PrintWriter out = res.getWriter();
		out.println(x);
	}
}

package qs.qgo.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.qs.qgo.repository.UnitListRepository;
import com.qs.qgo.repository.UnitListRepositoryImpl;

import qs.jco.service.SingletonComponent;

@Path("/units")
public class UnitList {

	@SingletonComponent
	private UnitListRepository unitListRepository;

	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String list() {
		unitListRepository = new UnitListRepositoryImpl();
		String response = unitListRepository.getUnitList();
		return response;
	}

}

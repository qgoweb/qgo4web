package com.qs.qgo.repository;

import qs.jco.manager.JCoManager;
import qs.jco.manager.JCoManagerImpl;

public class MenuListRepositoryImpl implements MenuListRepository {
	
	private static final String MENU_FUNCTION = "/QGO/WS_SUITE_DA_01_GET_LIST";
	private static final String MENU_JSON_PARAM = "YS_LIST_SUITE_DA_01_JSON";
	private JCoManager jcoManager = new JCoManagerImpl();
	
	@Override
	public String getMenuList() {
		return jcoManager.getData(MENU_FUNCTION, MENU_JSON_PARAM, null);
	}

}

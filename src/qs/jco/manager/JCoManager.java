package qs.jco.manager;

import java.util.Map;

public interface JCoManager {
	String getData(String functionName, String outputParameterName, Map<String, Object> params);
}

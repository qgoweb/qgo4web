package qs.jco.manager;


import java.util.Map;
import com.sap.conn.jco.JCoRecord;
import com.sap.conn.jco.ext.Environment;
import com.sap.conn.jco.ext.DestinationDataProvider;

public class JCoManagerImpl implements JCoManager {

	private JCoRfcClient rfcClient = new JCoRfcClientImpl();
	private DestinationDataProvider dataProvider = new  JCoDataProvider();

	public JCoManagerImpl() {
		if (!Environment.isDestinationDataProviderRegistered()) {
			Environment.registerDestinationDataProvider(dataProvider);
		}
	}

	public String getData(String functionName, String outputParameterName, Map<String, Object> params) {
		Map<String, JCoRecord> outRecords = rfcClient.executeRfc(functionName, params);
		JCoRecord outParams = outRecords.get(JCoListParameter.EXPORT_PARAMETERS_LIST);
		return outParams.getString(outputParameterName);
	}
}

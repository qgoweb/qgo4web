package com.qs.qgo.repository;

import java.util.HashMap;
import java.util.Map;

import qs.jco.manager.JCoManager;
import qs.jco.manager.JCoManagerImpl;

public class RunIdListRepositoryImpl implements RunIdListRepository {
	private static final String RUNID_FUNCTION = "/QGO/WS_RUNID_GET_LIST";
	private static final String RUNID_UNIT_PARAM_NAME = "X_UNIT";
	private static final String RUNID_JSON_PARAM_NAME = "YS_LIST_RUNID_JSON";
	
	private JCoManager jcoManager = new JCoManagerImpl();
	
	@Override
	public String getRunIdList(Object unitName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(RUNID_UNIT_PARAM_NAME, unitName);
		return jcoManager.getData(RUNID_FUNCTION, RUNID_JSON_PARAM_NAME, params);
	}

}

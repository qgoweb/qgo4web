package qs.jco.manager;

import java.util.HashMap;
import java.util.Map;

public class JCoConnectionCheck {
	private static final String CONNECTION_CHECK = "/QGO/WS_CONNECTION_VALID";
	private static final String CONNECTION_JSON_PARAM = "";
	private JCoManager jcoManager = new JCoManagerImpl();
	
	public JCoConnectionCheck(){}
	
	public String checkConnection(String requText) {
		Map<String, Object> inParams = new HashMap<String, Object>();
		inParams.put("REQUTEXT", requText);
		return jcoManager.getData(CONNECTION_CHECK, CONNECTION_JSON_PARAM, inParams);
	}
}

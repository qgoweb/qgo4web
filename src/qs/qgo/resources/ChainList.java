package qs.qgo.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.qs.qgo.repository.ChainRunListRepository;
import com.qs.qgo.repository.ChainRunListRepositoryImpl;

import qs.jco.service.SingletonComponent;

@Path("/chains")
public class ChainList {

	@SingletonComponent
	private ChainRunListRepository chainRunListRepository;

	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list(@QueryParam("unit") String unit, @QueryParam("runid") String runId,
			@QueryParam("pt") String processType) {
		chainRunListRepository = new ChainRunListRepositoryImpl();
		String response = chainRunListRepository.getChainRunList(unit, runId, processType);
		int i = 1;
		return response;
	}
}

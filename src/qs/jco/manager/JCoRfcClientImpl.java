package qs.jco.manager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoRecord;
import com.sap.conn.jco.JCoRuntimeException;

public class JCoRfcClientImpl implements JCoRfcClient {
	
	public Map<String, JCoRecord> executeRfc(String bapi, Map<String, Object> inParams) {
		Map<String, JCoRecord> records = new HashMap<String, JCoRecord>();
		try {
			JCoDestination jcoDestination = JCoDestinationManager.getDestination(JCoDataProvider.RFC_SYSTEM_ID);
			JCoFunction jcoFunction = jcoDestination.getRepository().getFunction(bapi);
			if (jcoFunction == null) {
				throw new JCoRuntimeException(JCoException.JCO_ERROR_FUNCTION_NOT_FOUND, bapi);
			}
			// see if we need to set the input parameters
			if (inParams != null) {
				// see if the function accepts any input parameters
				JCoParameterList jcoInParams = jcoFunction.getImportParameterList();
				if (jcoInParams == null) {
					throw new JCoRuntimeException(JCoException.JCO_ERROR_CONFIGURATION,
							"JCo function " + bapi + " does not have any input parameters");
				}
				// loop through the supplied parameter map and set the function
				// parameters
				for (Iterator<String> iter = inParams.keySet().iterator(); iter.hasNext();) {
					String key = iter.next();
					Object value = inParams.get(key);
					try {
						jcoInParams.setValue(key, value);
					} catch (JCoRuntimeException e) {
						throw new JCoRuntimeException(e.getGroup(), e.getKey(),
								"Problem setting input parameter " + key + " for bapi " + bapi + ". " + e.getMessage(),
								e);
					}
				}
			}
			// execute call
			jcoFunction.execute(jcoDestination);
			
			
			// set the export parameters
			JCoParameterList jcoOutParams = jcoFunction.getExportParameterList();
			if (jcoOutParams != null) {
				records.put(JCoListParameter.EXPORT_PARAMETERS_LIST, jcoOutParams);
			}
			// set the table parameters
			JCoParameterList jcoTabParams = jcoFunction.getTableParameterList();
			if (jcoTabParams != null) {
				records.put(JCoListParameter.TABLE_PARAMETERS_LIST, jcoTabParams);
			}
		} catch (JCoException e) {
			throw new JCoRuntimeException(e.getGroup(), e.getKey(), e.getMessage(), e);
		}
		return records;
	}
}

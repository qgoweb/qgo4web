package com.qs.qgo.repository;

public interface ChainRunListRepository {
	String getChainRunList(Object unitName, Object runId, Object processType);
}

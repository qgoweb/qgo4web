package qs.qgo.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import qs.jco.manager.JCoConnectionCheck;
import qs.jco.service.SingletonComponent;

@Path("/check")
public class CheckConnection {

	@SingletonComponent
	private JCoConnectionCheck connectionCheck;

	@Path("/connection")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String connection() {
		connectionCheck = new JCoConnectionCheck();
		String response = connectionCheck.checkConnection("Test");
		return response;
	}
}

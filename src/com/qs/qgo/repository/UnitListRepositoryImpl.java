package com.qs.qgo.repository;

import qs.jco.manager.JCoManager;
import qs.jco.manager.JCoManagerImpl;

public class UnitListRepositoryImpl implements UnitListRepository {
	private static final String UNIT_FUNCTION = "/QGO/WS_UNIT_GET_LIST";
	private static final String UNIT_JSON_PARAM = "YS_LIST_UNIT_JSON";
	private JCoManager jcoManager = new JCoManagerImpl();
	
	@Override
	public String getUnitList() {
		return jcoManager.getData(UNIT_FUNCTION, UNIT_JSON_PARAM, null);
	}

}
